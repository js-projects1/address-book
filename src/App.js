import React, { useState, useEffect } from 'react';

import axios from 'axios';


const Name=(props)=>{
	return(<div>
		<li> {props.name} {props.number} </li>
</div>	)
}

const App = () => {
// Define states

  const [ persons, setPersons ] = useState([]) 
  const [ newName, setNewName ] = useState('')
  const [ newSearch, setNewSearch ] = useState('')
  const [ newNumber, setNewNumber ] = useState('')
// Fetching data with axios and Effect Hooks==> In the asyncronous concept you put it at the beginning but ii is rendered at the end after by the API efect hook that
	//  put everything in the event queue and is shifted later in the stack.
  useEffect(() => {
    console.log('effect')
    axios
      .get('http://localhost:3001/persons')
      .then(response => {
        console.log('promise fulfilled')
        setPersons(response.data)
      })
  }, [])
    console.log('render', persons.length, 'persons')
// manage onSubmit  
const addNumber = (event)=>{
	  event.preventDefault()
	  // definisco un oggetto
	  const personObject = {
		  name: newName,
	          number: newNumber,
		  date: new Date().toISOString(),
		  id: persons.length +1,
	  }
	  // concateno alla persona precedente con il cambiamento di stato
	  console.log(persons.indexOf({newName}))
	  //Checking the presence of the same name (Remember that name is a propery of the elements of the vector) with conditional condition is verified? yes : no
	  persons.map(e => e.name).indexOf(newName) > -1 ?  alert(`${newName} is already added to phonebook`) : setPersons(persons.concat(personObject))
	  //libero il vettore
          setNewName('')
          setNewNumber('')
	  console.log('button clicked', event.target)
	}
   // Per aggiungere il nome
 
   const handleNewName = (event)=>{
		console.log(event.target.value)
		setNewName(event.target.value)
	} 
   const handleNewNumber = (event)=>{
		console.log(event.target.value)
		setNewNumber(event.target.value)
	}
    const handleNewSearch = (event)=>{
		console.log(event.target.value)
		setNewSearch(event.target.value)
	}

// Focus the cursor on a certain field
   const getfocus = ()=>{document.getElementById("name").focus()}

return (
    <div>
	<h2> Search </h2>
    <div>
	  search: <input id="search"
	  value={newSearch}
	  onChange={handleNewSearch}/> 
     </div>
     <ul>
	  {persons.filter(person=>person.name.toLowerCase().includes(newSearch.toLowerCase())).map(person=>
	  	<Name key={person.id} name={person.name} number={person.number}/>)}
     </ul>
     <div>
      <h2>Phonebook</h2>
      <form onSubmit={addNumber}>
        <div>
          name: <input id="name"
	  value={newName}
	  onChange={handleNewName}/>
        </div>
        <div>
          number: <input id="number"
	  value={newNumber}
	  onChange={handleNewNumber}/>
        </div>
	<div>
          <button type="submit" onClick={getfocus}>add</button>
        </div>
      </form>
      <h2>Numbers</h2>
      <ul>
	  {persons.map(person=>
	  	<Name key={person.id} name={person.name} number={person.number}/> // Uso un altro elemento che fa da lista
	  )}
     </ul>
    </div>
    </div>
  )
}

export default App;
